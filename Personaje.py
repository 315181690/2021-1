class Personaje:
    
    _posicionX = 0
    _posicionY = 0
    _velocidad = 1
    _vida = 3
     
    def __init__(self):
        self._posicionX = 0
        self._posicionY = 0
        self._velocidad = 1
        self._vida = 3

    
    def _tomaEstrellita(self):
        self._velocidad = 2
    
    def _acabaEstrellita(self):
        self._velocidad = 1
        
    def _mueveIzquierda(self):
        if self._velocidad == 2 :
            self._posicionX -= 2
        else:
            self._posicionX -= 1

    def _mueveDerecha(self):
        if self._velocidad == 2 :
            self._posicionX += 2
        else:
            self._posicionX += 1            
            
    def _damage(self):
        self._vida -= 1
        
    def _heal(self):
        self._vida += 1
            
    def _salta(self):
        self._posicionY += 1
    
    def _cae(self):
        self._posicionY -= 1
        
    def botonDerecha(self):
        self._mueveDerecha()

    def botonIzquierda(self):
        self._mueveIzquierda()
    
    def botonX(self):
        self._salta
        
    def interactua(self, objeto):
        if objeto == "enemigo":
            self._damage()
        if objeto == "pocion":
            self._heal()
        if objeto == "estrella":
            self._tomaEstrellita()
        
    def estado(self):
        print ("("+str(self._posicionX)+","+str(self._posicionY)+"), Vida:"+str(self._vida))


personaje = Personaje()#Se crea personaje
personaje.estado()#se imprime el estado inicial del personaje
personaje.botonDerecha()#se mueve a la derecha
personaje.estado()
personaje.interactua("estrella")
personaje.botonDerecha()
personaje.estado()
personaje.interactua("enemigo")
personaje.estado()
personaje.interactua("enemigo")
personaje.estado()
personaje.interactua("pocion")
personaje.estado()
personaje._heal()
personaje.estado()
