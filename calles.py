__author__ = "Silhi Bernabe"

n=int(input("Ingresa el número de calles verticales: "))
m=int(input("Ingresa el número de calles horizontales: "))

# Primero, haremos un algoritmo recursivo para crear el triangulo de Pascal
h=(m+1)+(n+1)
aux=m+1
def pascal(h, col):
    if(col==1): return 1
    if(col==h): return 1
    arrIzq = pascal(h-1, col-1) # Se llama a si mismo para sumar el valor de arriba a la izquierda
    arrDer = pascal(h-1, col)   # con el de arriba a la derecha
    return arrIzq+arrDer

# Imprimimos el triangulo de Pascal
print("La configuracion del respectivo triangulo de Pascal es:")
for r in range (1, h):
    for c in range (1, r+1):
        print(pascal(r,c), end="  ")
        if (c==aux):
            combinaciones=pascal(r,c) # Este es el punto buscado, en donde se encuentra el punto A
    print("")

# Damos el resultado, que se encontro en la seccion anterior
print("El numero de posibles caminos que se pueden tomar desde el punto A al punto B es: "+str(combinaciones))
